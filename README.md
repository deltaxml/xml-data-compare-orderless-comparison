# Using a Key for Ordering

See [the sample page](https://docs.deltaxml.com/xdc/latest/samples/orderless-comparison) on the website for more details about ordered and orderless comparisons.

Resources found here include pairs of input XML files for 'contacts' or 'addresses'.
There are also two config files for performing a keyed (config-ignore-order-with-key.xml) or non-keyed (config-ignore-order-no-key.xml) orderless comparison.


## REST request:

To invoke comparisons using the XML Data Compare REST API use the following XML in the body of the request. Replacing {LOCATION} below with your location of the download. 

Example 1 (keyed using addresses files):


```
<compare>
    <inputA xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/addressesA.xml</path>
    </inputA>
    <inputB xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/addressesB.xml</path>
    </inputB>
    <configuration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/config-ignore-order-with-key.xml</path>
    </configuration>
</compare>
```

Example 2 (non-keyed using contacts files):

```
<compare>
    <inputA xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/contactsA.xml</path>
    </inputA>
    <inputB xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/contactsB.xml</path>
    </inputB>
    <configuration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/config-ignore-order-no-key.xml</path>
    </configuration>
</compare>
```



See [XML Data Compare REST User Guide](https://docs.deltaxml.com/xdc/latest/rest-user-guide) for how to run the comparison using REST.

